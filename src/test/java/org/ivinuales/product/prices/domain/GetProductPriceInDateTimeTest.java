package org.ivinuales.product.prices.domain;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class GetProductPriceInDateTimeTest {

    private static LocalDateTime buildLocalDateTime(long epocTimestamp) {
        return LocalDateTime.ofInstant(Instant.ofEpochMilli(epocTimestamp), ZoneId.systemDefault());
    }

    @Test
    public void when_repository_is_null_return_empty() {

        var emptyValue = new GetProductPriceInDateTime(null).getMoreSignificantPrice(1, 1, null);
        Assertions.assertNotNull(emptyValue);
        Assertions.assertFalse(emptyValue.isPresent());
    }

    @Test
    public void get_unique_product_price_in_dateTime() {

        int brandId = 1;
        int productId = 1;
        LocalDateTime requestTimestamp = LocalDateTime.ofInstant(Instant.ofEpochMilli(System.currentTimeMillis()), ZoneId.systemDefault());

        ArrayList<ProductPrice> repositoryData = new ArrayList<>();
        repositoryData.add(
                new ProductPrice(1, 1, buildLocalDateTime(111111), buildLocalDateTime(222222), 35.50, 1, 1, "EUR")
        );

        GetProductPriceInDateTimeRepository repository = mock(GetProductPriceInDateTimeRepository.class);
        when(repository.findProductPricesInDateTime(brandId, productId, requestTimestamp)).thenReturn(repositoryData);

        var productPrice = new GetProductPriceInDateTime(repository).getMoreSignificantPrice(brandId, productId, requestTimestamp);

        Assertions.assertTrue(productPrice.isPresent());
        Assertions.assertEquals(repositoryData.get(0).getBrandId(), productPrice.get().getBrandId());
        Assertions.assertEquals(repositoryData.get(0).getProductId(), productPrice.get().getProductId());
        Assertions.assertEquals(repositoryData.get(0).getPriceList(), productPrice.get().getPriceList());
        Assertions.assertEquals(repositoryData.get(0).getPrice(), productPrice.get().getPrice(), 0);
        Assertions.assertEquals(repositoryData.get(0).getInitTimestamp(), productPrice.get().getInitTimestamp());
        Assertions.assertEquals(repositoryData.get(0).getEndTimestamp(), productPrice.get().getEndTimestamp());
    }

    @Test
    public void get_more_significant_product_price_in_dateTime() {

        int brandId = 1;
        int productId = 1;
        LocalDateTime requestTimestamp = LocalDateTime.ofInstant(Instant.ofEpochMilli(System.currentTimeMillis()), ZoneId.systemDefault());

        ArrayList<ProductPrice> repositoryData = new ArrayList<>();
        repositoryData.add(
                new ProductPrice(1, 1, buildLocalDateTime(111111), buildLocalDateTime(222222), 35.50, 1, 1, "EUR")
        );
        repositoryData.add(
                new ProductPrice(1, 1, buildLocalDateTime(111111), buildLocalDateTime(222222), 55.44, 1, 0, "EUR")
        );

        GetProductPriceInDateTimeRepository repository = mock(GetProductPriceInDateTimeRepository.class);
        when(repository.findProductPricesInDateTime(brandId, productId, requestTimestamp)).thenReturn(repositoryData);

        var productPrice = new GetProductPriceInDateTime(repository).getMoreSignificantPrice(brandId, productId, requestTimestamp);

        Assertions.assertTrue(productPrice.isPresent());
        Assertions.assertEquals(repositoryData.get(0).getBrandId(), productPrice.get().getBrandId());
        Assertions.assertEquals(repositoryData.get(0).getProductId(), productPrice.get().getProductId());
        Assertions.assertEquals(repositoryData.get(0).getPriceList(), productPrice.get().getPriceList());
        Assertions.assertEquals(repositoryData.get(0).getPrice(), productPrice.get().getPrice(), 0);
        Assertions.assertEquals(repositoryData.get(0).getInitTimestamp(), productPrice.get().getInitTimestamp());
        Assertions.assertEquals(repositoryData.get(0).getEndTimestamp(), productPrice.get().getEndTimestamp());
    }

    @Test
    public void get_no_product_price_in_dateTime() {

        int brandId = 1;
        int productId = 1;
        LocalDateTime requestTimestamp = LocalDateTime.ofInstant(Instant.ofEpochMilli(System.currentTimeMillis()), ZoneId.systemDefault());

        ArrayList<ProductPrice> repositoryData = new ArrayList<>();

        GetProductPriceInDateTimeRepository repository = mock(GetProductPriceInDateTimeRepository.class);
        when(repository.findProductPricesInDateTime(brandId, productId, requestTimestamp)).thenReturn(repositoryData);

        var productPrice = new GetProductPriceInDateTime(repository).getMoreSignificantPrice(brandId, productId, requestTimestamp);

        Assertions.assertFalse(productPrice.isPresent());
    }
}
