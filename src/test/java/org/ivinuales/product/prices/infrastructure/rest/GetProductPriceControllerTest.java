package org.ivinuales.product.prices.infrastructure.rest;

import org.ivinuales.product.prices.application.ProductPriceDTO;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class GetProductPriceControllerTest {

    @Autowired
    private TestRestTemplate restTemplate;

    @LocalServerPort
    private int port;

    @Test
    public void response_notFound_httpCode_when_product_price_not_found() {

        String dateTime = "2020-06-14T16:00:00";
        int brandId = 2;
        int productId = 2;

        String queryParams = String.format("/product/price?brandId=%s&productId=%s&dateTime=%s",
                brandId,
                productId,
                dateTime);

        var response = restTemplate.exchange(String.format("http://localhost:%s/%s", port, queryParams), HttpMethod.GET, null, String.class);

        Assertions.assertNotNull(response);
        Assertions.assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

    @Test
    public void response_badRequest_httpCode_when_product_is_invalid_value() {

        String dateTime = "2020-06-14T16:00:00";
        int brandId = 1;
        int productId = 0;

        String queryParams = String.format("/product/price?brandId=%s&productId=%s&dateTime=%s",
                brandId,
                productId,
                dateTime);

        var response = restTemplate.exchange(String.format("http://localhost:%s/%s", port, queryParams), HttpMethod.GET, null, String.class);

        Assertions.assertNotNull(response);
        Assertions.assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }

    @Test
    public void response_badRequest_httpCode_when_brand_is_invalid_value() {

        String dateTime = "2020-06-14T16:00:00";
        int brandId = 0;
        int productId = 1;

        String queryParams = String.format("/product/price?brandId=%s&productId=%s&dateTime=%s",
                brandId,
                productId,
                dateTime);

        var response = restTemplate.exchange(String.format("http://localhost:%s/%s", port, queryParams), HttpMethod.GET, null, String.class);

        Assertions.assertNotNull(response);
        Assertions.assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }

    @Test
    public void response_badRequest_httpCode_when_dateTime_is_invalid_date() {

        String dateTime = "2020-06-88T16:00:00";
        int brandId = 0;
        int productId = 0;

        String queryParams = String.format("/product/price?brandId=%s&productId=%s&dateTime=%s",
                brandId,
                productId,
                dateTime);

        var response = restTemplate.exchange(String.format("http://localhost:%s/%s", port, queryParams), HttpMethod.GET, null, String.class);

        Assertions.assertNotNull(response);
        Assertions.assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/acceptance_test.csv", numLinesToSkip = 1)
    public void run_acceptance_test_from_resources(int brandId, int productId, String dateTime, double expectedPrice, String expectedCurrency) {

        LocalDateTime requestDateTime = LocalDateTime.parse(dateTime);

        String queryParams = String.format("/product/price?brandId=%s&productId=%s&dateTime=%s",
                brandId,
                productId,
                dateTime);

        var response = this.restTemplate.getForObject(String.format("http://localhost:%s/%s", port, queryParams), ProductPriceDTO.class);

        Assertions.assertNotNull(response);
        Assertions.assertEquals(brandId, response.getBrandId());
        Assertions.assertEquals(productId, response.getProductId());
        Assertions.assertEquals(expectedPrice, response.getPrice(), 0);
        Assertions.assertTrue(response.getStartDateTime().isBefore(requestDateTime));
        Assertions.assertTrue(response.getEndDateTime().isAfter(requestDateTime));
        Assertions.assertEquals(expectedCurrency, response.getCurrency());
    }
}
