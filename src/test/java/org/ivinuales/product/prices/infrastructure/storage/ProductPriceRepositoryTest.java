package org.ivinuales.product.prices.infrastructure.storage;

import org.ivinuales.product.prices.Main;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest(classes = Main.class)
public class ProductPriceRepositoryTest {

    @Autowired
    ProductPriceRepository repository;

    @Test
    public void initial_data_was_loaded() {

        var all = repository.findAll();
        assertNotNull(all);
        assertEquals(4, all.size());
    }

    @Test
    public void create_product_price_in_repository() {

        long timestamp = System.currentTimeMillis();


        var saved = repository.save(new ProductPriceEntity(1, 1,
                LocalDateTime.ofInstant(Instant.ofEpochMilli(0), ZoneId.systemDefault()),
                LocalDateTime.ofInstant(Instant.ofEpochMilli(timestamp), ZoneId.systemDefault()),
                0, 0, 11.22, "EUR"));

        var found = repository.getOne(saved.getId());

        assertNotNull(saved);
        assertNotNull(found);
        assertEquals(saved.getId(), found.getId());

        repository.delete(found);
    }

    @Test
    public void recovery_product_prices_from_brandId_productId_and_timestamp() {

        int brandId = 1;
        int productId = 35455;
        //2020-06-14
        LocalDateTime localDateTime = LocalDateTime.of(2020, 6, 14, 17, 0);

        var founds = repository.findAllByBrandIdProductIdAndDateTime(brandId, productId, localDateTime);

        assertNotNull(founds);
        assertEquals(2, founds.size());
    }
}
