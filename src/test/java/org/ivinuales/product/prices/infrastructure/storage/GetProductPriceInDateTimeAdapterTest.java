package org.ivinuales.product.prices.infrastructure.storage;

import org.ivinuales.product.prices.Main;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(classes = Main.class)
public class GetProductPriceInDateTimeAdapterTest {

    @Autowired
    GetProductPriceInDateTimeAdapter adapter;

    @Test
    public void when_repository_is_null_return_emtpy_list() {

        var emptyList = new GetProductPriceInDateTimeAdapter(null).findProductPricesInDateTime(1, 1, null);
        assertNotNull(emptyList);
        assertEquals(0, emptyList.size());
    }

    @Test
    public void recovery_product_prices_from_brandId_and_productId_and_timestamp() {

        int brandId = 1;
        int productId = 35455;
        //2020-06-14
        LocalDateTime localDateTime = LocalDateTime.of(2020, 6, 14, 17, 0);

        var founds = adapter.findProductPricesInDateTime(brandId, productId, localDateTime);

        assertNotNull(founds);
        assertEquals(2, founds.size());
    }

}
