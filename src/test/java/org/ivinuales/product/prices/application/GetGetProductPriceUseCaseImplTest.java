package org.ivinuales.product.prices.application;

import org.ivinuales.product.prices.Main;
import org.ivinuales.product.prices.domain.ProductPrice;
import org.ivinuales.product.prices.infrastructure.storage.GetProductPriceInDateTimeAdapter;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.security.InvalidParameterException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;

@SpringBootTest(classes = Main.class)
public class GetGetProductPriceUseCaseImplTest {

    @Autowired
    GetProductPriceUseCase useCase;

    @Mock
    GetProductPriceInDateTimeAdapter adapter;

    @Test
    public void when_recovery_null_no_fails() {

        LocalDateTime requestTimestamp = LocalDateTime.ofInstant(Instant.ofEpochMilli(System.currentTimeMillis()), ZoneId.systemDefault());

        var result = useCase.getPriceInDateTime(1, 1, requestTimestamp);

        Assertions.assertNull(result);
    }

    @Test
    public void when_repository_is_null_return_null() {

        LocalDateTime requestTimestamp = LocalDateTime.ofInstant(Instant.ofEpochMilli(System.currentTimeMillis()), ZoneId.systemDefault());

        var nullValue = new GetProductPriceUseCaseImpl(null).getPriceInDateTime(1, 1, requestTimestamp);
        Assertions.assertNull(nullValue);
    }

    @Test
    public void when_dateTime_is_null_throw_exception() {

        try {
            var noValue = new GetProductPriceUseCaseImpl(null).getPriceInDateTime(1, 1, null);
            Assertions.fail("It should throw an exception");
        } catch (InvalidParameterException invalidParameterException) {

            Assertions.assertNotNull(invalidParameterException);
        }
    }

    @Test
    public void when_brandId_is_zero_throw_exception() {

        LocalDateTime requestTimestamp = LocalDateTime.ofInstant(Instant.ofEpochMilli(System.currentTimeMillis()), ZoneId.systemDefault());
        try {
            var noValue = new GetProductPriceUseCaseImpl(null).getPriceInDateTime(0, 1, requestTimestamp);
            Assertions.fail("It should throw an exception");
        } catch (InvalidParameterException invalidParameterException) {

            Assertions.assertNotNull(invalidParameterException);
        }
    }

    @Test
    public void when_productId_is_zero_throw_exception() {

        LocalDateTime requestTimestamp = LocalDateTime.ofInstant(Instant.ofEpochMilli(System.currentTimeMillis()), ZoneId.systemDefault());
        try {
            var noValue = new GetProductPriceUseCaseImpl(null).getPriceInDateTime(1, 0, requestTimestamp);
            Assertions.fail("It should throw an exception");
        } catch (InvalidParameterException invalidParameterException) {

            Assertions.assertNotNull(invalidParameterException);
        }
    }

    @Test
    public void when_recovery_price_return_price() {

        LocalDateTime requestTimestamp = LocalDateTime.ofInstant(Instant.ofEpochMilli(System.currentTimeMillis()), ZoneId.systemDefault());

        ArrayList<ProductPrice> repositoryData = new ArrayList<>();
        repositoryData.add(new ProductPrice(1, 1, null, null, 35.50, 1, 1, null));

        Mockito.when(adapter.findProductPricesInDateTime(1, 1, requestTimestamp)).thenReturn(repositoryData);

        var result = new GetProductPriceUseCaseImpl(adapter).getPriceInDateTime(1, 1, requestTimestamp);

        Assertions.assertNotNull(result);
        Assertions.assertEquals(repositoryData.get(0).getBrandId(), result.getBrandId());
        Assertions.assertEquals(repositoryData.get(0).getProductId(), result.getProductId());
        Assertions.assertEquals(repositoryData.get(0).getPriceList(), result.getPriceList());
        Assertions.assertEquals(repositoryData.get(0).getPrice(), result.getPrice(), 0);
        Assertions.assertEquals(repositoryData.get(0).getInitTimestamp(), result.getStartDateTime());
        Assertions.assertEquals(repositoryData.get(0).getEndTimestamp(), result.getEndDateTime());
    }
}
