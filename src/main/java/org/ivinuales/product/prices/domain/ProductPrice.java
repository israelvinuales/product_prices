package org.ivinuales.product.prices.domain;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDateTime;

@AllArgsConstructor
@Data
public class ProductPrice {

    int brandId;
    int productId;
    LocalDateTime initTimestamp;
    LocalDateTime endTimestamp;
    double price;
    int priceList;
    int priority;
    String currency;
}
