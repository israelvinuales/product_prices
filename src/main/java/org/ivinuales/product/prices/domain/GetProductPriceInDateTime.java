package org.ivinuales.product.prices.domain;

import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public class GetProductPriceInDateTime {

    private final GetProductPriceInDateTimeRepository repository;

    public GetProductPriceInDateTime(GetProductPriceInDateTimeRepository repository) {
        this.repository = repository;
    }

    public Optional<ProductPrice> getMoreSignificantPrice(int brandId, int productId, LocalDateTime dateTime) {

        if (repository == null) return Optional.empty();

        List<ProductPrice> prices = repository.findProductPricesInDateTime(brandId, productId, dateTime);

        return getProductPriceWithMaxPriority(prices);
    }

    private Optional<ProductPrice> getProductPriceWithMaxPriority(List<ProductPrice> prices) {
        return prices.stream().max(Comparator.comparing(ProductPrice::getPriority));
    }
}
