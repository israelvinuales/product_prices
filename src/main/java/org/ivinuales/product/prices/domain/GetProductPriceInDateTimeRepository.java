package org.ivinuales.product.prices.domain;

import java.time.LocalDateTime;
import java.util.List;

public interface GetProductPriceInDateTimeRepository {

    List<ProductPrice> findProductPricesInDateTime(int brandId, int productId, LocalDateTime dateTime);
}
