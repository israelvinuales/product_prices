package org.ivinuales.product.prices.infrastructure.storage;

import org.ivinuales.product.prices.domain.ProductPrice;

public abstract class ProductPriceMapper {

    public static ProductPrice toProductPrice(ProductPriceEntity entity) {

        return new ProductPrice(entity.getBrandId(), entity.getProductId(), entity.getStartDate(), entity.getEndDate(), entity.getPrice(), entity.getPriceList(), entity.getPriority(), entity.getCurrency());
    }
}
