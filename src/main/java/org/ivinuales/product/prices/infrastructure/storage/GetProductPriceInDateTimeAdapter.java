package org.ivinuales.product.prices.infrastructure.storage;

import org.ivinuales.product.prices.domain.GetProductPriceInDateTimeRepository;
import org.ivinuales.product.prices.domain.ProductPrice;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class GetProductPriceInDateTimeAdapter implements GetProductPriceInDateTimeRepository {

    private final ProductPriceRepository repository;

    public GetProductPriceInDateTimeAdapter(ProductPriceRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<ProductPrice> findProductPricesInDateTime(int brandId, int productId, LocalDateTime dateTime) {

        if (repository == null) return Collections.emptyList();
        List<ProductPriceEntity> list = repository.findAllByBrandIdProductIdAndDateTime(brandId, productId, dateTime);
        if (list == null) return Collections.emptyList();
        return list.stream().map(ProductPriceMapper::toProductPrice).collect(Collectors.toList());
    }
}
