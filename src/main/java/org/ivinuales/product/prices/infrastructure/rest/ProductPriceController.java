package org.ivinuales.product.prices.infrastructure.rest;

import org.ivinuales.product.prices.application.GetProductPriceUseCase;
import org.ivinuales.product.prices.application.ProductPriceDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.time.LocalDateTime;

@RestController
@RequestMapping(value = "product/price")
public class ProductPriceController {

    @Autowired
    GetProductPriceUseCase getProductPriceUseCase;

    @GetMapping
    public ProductPriceDTO getPrice(@RequestParam(value = "brandId") int brandId,
                                    @RequestParam(value = "productId") int productId,
                                    @RequestParam(value = "dateTime") String strDateTime) {

        ProductPriceDTO response;
        try {
            LocalDateTime localDateTime = LocalDateTime.parse(strDateTime);
            response = getProductPriceUseCase.getPriceInDateTime(brandId, productId, localDateTime);
        } catch (Exception ex) {

            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Request parameters are invalid");
        }
        if (response == null) throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Product price not found");
        return response;
    }
}
