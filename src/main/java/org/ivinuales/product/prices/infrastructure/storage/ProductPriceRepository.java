package org.ivinuales.product.prices.infrastructure.storage;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.time.LocalDateTime;
import java.util.List;

public interface ProductPriceRepository extends JpaRepository<ProductPriceEntity, Integer> {

    @Query("select p from ProductPriceEntity p where p.brandId = ?1 and p.productId = ?2 and ?3 between p.startDate and p.endDate")
    List<ProductPriceEntity> findAllByBrandIdProductIdAndDateTime(int brandId, int productId, LocalDateTime time);
}
