package org.ivinuales.product.prices.application;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.ivinuales.product.prices.domain.ProductPrice;

import java.time.LocalDateTime;

@AllArgsConstructor
@Data
public class ProductPriceDTO {

    private int brandId;
    private int productId;
    private LocalDateTime startDateTime;
    private LocalDateTime endDateTime;
    private int priceList;
    private double price;
    private String currency;

    public static ProductPriceDTO from(ProductPrice productPrice) {

        if (productPrice == null) return null;
        return new ProductPriceDTO(productPrice.getBrandId(), productPrice.getProductId(), productPrice.getInitTimestamp(), productPrice.getEndTimestamp(), productPrice.getPriceList(), productPrice.getPrice(), productPrice.getCurrency());
    }
}
