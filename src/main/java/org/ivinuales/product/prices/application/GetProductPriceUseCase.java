package org.ivinuales.product.prices.application;

import java.security.InvalidParameterException;
import java.time.LocalDateTime;

public interface GetProductPriceUseCase {

    ProductPriceDTO getPriceInDateTime(int brandId, int productId, LocalDateTime dateTime) throws InvalidParameterException;
}
