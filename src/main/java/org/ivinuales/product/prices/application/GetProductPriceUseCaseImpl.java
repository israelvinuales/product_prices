package org.ivinuales.product.prices.application;

import org.ivinuales.product.prices.domain.GetProductPriceInDateTime;
import org.ivinuales.product.prices.infrastructure.storage.GetProductPriceInDateTimeAdapter;
import org.springframework.stereotype.Service;

import java.security.InvalidParameterException;
import java.time.LocalDateTime;

@Service
public class GetProductPriceUseCaseImpl implements GetProductPriceUseCase {

    private final GetProductPriceInDateTimeAdapter adapter;

    public GetProductPriceUseCaseImpl(GetProductPriceInDateTimeAdapter adapter) {
        this.adapter = adapter;
    }

    @Override
    public ProductPriceDTO getPriceInDateTime(int brandId, int productId, LocalDateTime dateTime) throws InvalidParameterException {

        if (!validateRequestParameters(brandId, productId, dateTime)) throw new InvalidParameterException();

        var price = new GetProductPriceInDateTime(adapter).getMoreSignificantPrice(brandId, productId, dateTime);

        return price.map(ProductPriceDTO::from).orElse(null);
    }

    private boolean validateRequestParameters(int brandId, int productId, LocalDateTime dateTime) {

        if (brandId < 1) return false;
        if (productId < 1) return false;
        if (dateTime == null) return false;
        return true;
    }
}
