# Product prices

Api Rest para obtener el precio de un producto en una fecha concreta.

## Dependencias
El servicio hace uso de las siguientes librerías:
* lombok
* h2
* spring-boot-starter-web
* spring-boot-starter-data-jpa
* spring-boot-starter-test
* springfox-swagger2
* springfox-swagger-ui

## Build & Run

Para compilar y desplegar el servicio desde una terminal debemos ejecutar lo siguiente.
> mvn package -f pom.xml
> 
> java -jar target/product.prices-1.0.0.jar 


Ahora el servicio está escuchando en el puerto 8080.

Se ha generado un documento [swagger-ui.html](http://localhost:8080/swagger-ui.html) para documentar el API del servicio

## Ejecutar tests de aceptación
Los tests de aceptación definidos se encuentran en el fichero Prices_acceptance_test.postman_collection.json de la raiz del proyecto.

Para ejecutarlos puede importarse a una instancia de PostMan y ejecutarlos o bien lanzarlos mediante NewMan

> npm install -g newman
>
> newman run ./acceptance_tests/Prices_acceptance_test.postman_collection.json


## Estructura del proyecto

Se define la siguiente estructura:

### Domain
En esta capa se implementará la lógica propia del negocio, independiente de la aplicación que se desarrolle y las tecnologías/frameworks utilizadas.

Se define la siguiente regla de negocio:

*"Se requiere encontrar el precio de venta para un producto de una cadena en un momento del tiempo determinado"*

Implementación de la regla:
> i. Recuperar los precios disponibles para un momento del tiempo determinado, un producto y una cadena.
>
> ii. Entre los precios disponibles se recuperará aquel que tenga mayor prioridad

### Infrastructure
En esta capa se implementarán las piezas necesarias para comunicar la aplicación con el mundo exterior.

Proporcionará a los objetos de negocio todos aquellos datos (o acciones) que requieran de tecnologías específicas (almacenamiento, pub/sub, email, ftp ... ) de forma que Domain quede limpio de dependencias externas.

En este caso se implementa lo siguiente

#### Storage
Capa de almacenamiento de datos que proporciona a la aplicación un Adapter para obtener los datos que necesite

> i. ProductPriceEntity como entidad vinculada a Jpa de spring
>
> ii. ProductPriceRepository como repositorio de datos
>
> iii. GetProductPriceInDateTimeAdapter que es el encargado de adaptar los datos del repositorio a las necesidades del objeto de negocio
> 
> iv. ProductPriceMapper como mapeador entre ProductPriceEntity (modelo de persistencia) y ProductPrice (modelo de dominio)


#### Rest
Esta capa presenta una API Rest con un único punto de entrada /product/price con los siguientes parámetros de entrada

| Nombre | Tipo |
|------|------|
| BrandId | Numero entero mayor de 0
| ProductId | Numero entero mayor de 0
| DateTime | Fecha y hora UTC en formato YYYY-MM-DDThh:mm:ss

```java
public ProductPriceDTO getPrice(@RequestParam(value = "brandId") int brandId,
                                    @RequestParam(value = "productId") int productId,
                                    @RequestParam(value = "dateTime") String strDateTime) throws ResponseStatusException {
```

La respuesta será un objeto de tipo ProductPriceDTO o un codigo http 404 en caso de no encontrar ningun precio valido

```java
public class ProductPriceDTO {

    private int brandId;
    private int productId;
    private LocalDateTime startDateTime;
    private LocalDateTime endDateTime;
    private int priceList;
    private double price;
    private String currency;
    ...
}
```

### Application
Esta capa es el punto de entrada al servicio, ejerce de coordinador entre las piezas de infraestructura y los objetos de negocio.

Se implementa un único caso de uso *GetProductPriceUseCase*